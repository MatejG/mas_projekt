# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\Users\bujak\Desktop\FER\LRI1\MAS\Vitis\CameraPynq\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\Users\bujak\Desktop\FER\LRI1\MAS\Vitis\CameraPynq\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {CameraPynq}\
-hw {C:\Users\bujak\Desktop\FER\LRI1\MAS\projTest\design_1_wrapper.xsa}\
-proc {ps7_cortexa9_0} -os {standalone} -fsbl-target {psu_cortexa53_0} -out {C:/Users/bujak/Desktop/FER/LRI1/MAS/Vitis}

platform write
platform generate -domains 
platform active {CameraPynq}
bsp reload
bsp setlib -name lwip211 -ver 1.3
bsp write
bsp reload
catch {bsp regenerate}
platform generate
bsp config stdin "ps7_uart_0"
bsp reload
platform active {CameraPynq}
bsp reload
bsp write
platform generate
platform active {CameraPynq}
bsp reload
platform generate -domains 
bsp reload
platform generate -domains 
bsp reload
platform generate -domains 
platform generate -domains standalone_domain 
platform generate -domains standalone_domain 
platform active {CameraPynq}
bsp reload
platform generate -domains 
platform generate -domains standalone_domain 
platform active {CameraPynq}
bsp reload
bsp config dependency_flags "-MMD -MP"
bsp config dependency_flags "-MMD -MP"
bsp config extra_compiler_flags "-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -nostartfiles -g -Wall -Wextra"
bsp config dependency_flags "-MMD -MP"
bsp config extra_compiler_flags "-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -nostartfiles -g -Wall -Wextra -"
bsp config extra_compiler_flags "-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -nostartfiles -g -Wall -Wextra -lm"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains standalone_domain 
platform generate -domains standalone_domain 
platform active {CameraPynq}
bsp reload
bsp config extra_compiler_flags "-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -nostartfiles -g -Wall -Wextra"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains standalone_domain 
platform generate
platform generate
platform generate -domains standalone_domain 
platform generate
platform clean
platform generate
platform generate
platform clean
platform generate
platform generate
platform generate
platform generate -domains standalone_domain 
platform active {CameraPynq}
bsp reload
bsp config dependency_flags "-MMD -MP"
bsp config extra_compiler_flags "-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -nostartfiles -g -Wall -lm -Wextra"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains standalone_domain 
platform generate
platform active {CameraPynq}
bsp reload
bsp reload
platform generate -domains 
