/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#define TJE_IMPLEMENTATION
#include "cameraAPI_zynq.h"
#include "jpeg_encoder.h"
#define width 640
#define height 480


#include "lwip/err.h"
#include "lwip/tcp.h"
#if defined (__arm__) || defined (__aarch64__)
#include "xil_printf.h"
#endif

unsigned long length;
unsigned char *encriptMsg;
jpec_enc_t *enc;

volatile int slanje = 0;
volatile long alreadySent,sendBuffAll;

int transfer_data() {
	return 0;
}

void print_app_header()
{
	xil_printf("\n\r\n\r-----lwIP TCP echo server ------\n\r");
	xil_printf("TCP packets sent to port 6001 will be echoed back\n\r");
}

err_t sent_callback(void *arg, struct tcp_pcb *tpcb, u16_t len){
	int sendbuf = tcp_sndbuf(tpcb);
	if(slanje==0) return -1;
	if(alreadySent+sendbuf >= length){
		tcp_write(tpcb,encriptMsg+alreadySent,length-alreadySent+1,1);
		tcp_close(tpcb);
		slanje=0;
		//xil_printf("%d\r\n",alreadySent);
		xil_printf("Frame sent\r\n");
		free(encriptMsg);
		/* Release the encoder */
		jpec_enc_del(enc);
		return -1;
	}else{
		//xil_printf("alreadySent:%ld\r\n",alreadySent);
		tcp_write(tpcb, encriptMsg+alreadySent, sendbuf,1);
		alreadySent+=sendbuf;
		//alreadySent += sendbuf;
		return 0;
	}
	return 0;
}

err_t recv_callback(void *arg, struct tcp_pcb *tpcb,
                               struct pbuf *p, err_t err)
{

	int i,j;
	/* do not read the packet if we are not in ESTABLISHED state */
	if (!p) {
		tcp_close(tpcb);
		tcp_recv(tpcb, NULL);
		return ERR_OK;
	}

	/* indicate that the packet has been received */
	tcp_recved(tpcb, p->len);
	tcp_sent(tpcb, sent_callback);

	unsigned char* frameYUV = Camera_getFrame();

	unsigned char* frameGray = (unsigned char*)malloc(height*width*sizeof(unsigned char));

	//YUV422 format: Y1,U, Y2, V    => obraduju se 2 piksela u jednoj iteraciji
	//koristi se greyscale jer je slika naran�asta u boji
	long k = 0;
	for(i=0;i<height; i++){
		for(j=0;j<width*2;j+=2){
			frameGray[k] = frameYUV[i*width*2+j];//frame[i][j];
			k=k+1;
		}
	}
	free(frameYUV);
	//jpeg kodiranje
	enc = jpec_enc_new(frameGray, 640, 480);

	/* Compress */
	uint8_t *jpeg = jpec_enc_run(enc, &length);
	free(frameGray);
	unsigned char str[10] = {"##########"} ;
	sprintf(str, "%ld#", length);
	err = tcp_write(tpcb, str ,10,1);//po�alji prvo veli�inu slike

	encriptMsg = (unsigned char*)malloc(length*sizeof(unsigned char));
	for(int i=0;i<length;i++){
		encriptMsg[i] = jpeg[i] + 77;
	}
	int sendbuf = tcp_sndbuf(tpcb);

	/* in this case, we assume that the payload is < TCP_SND_BUF */
	if (tcp_sndbuf(tpcb) > p->len) {
		err = tcp_write(tpcb, encriptMsg ,sendbuf,1);
		alreadySent = sendbuf;

		slanje = 1;
	} else
		xil_printf("no space in tcp_sndbuf\n\r");
	/* free the received pbuf */
	pbuf_free(p);

	return ERR_OK;
}

err_t accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err)
{
	static int connection = 1;

	/* set the receive callback for this connection */
	tcp_recv(newpcb, recv_callback);

	/* just use an integer number indicating the connection id as the
	   callback argument */
	tcp_arg(newpcb, (void*)connection);

	/* increment for subsequent accepted connections */
	connection++;

	return ERR_OK;
}


int start_application()
{
	struct tcp_pcb *pcb;
	err_t err;
	unsigned port = 1077;

	/* create new TCP PCB structure */
	pcb = tcp_new();
	if (!pcb) {
		xil_printf("Error creating PCB. Out of Memory\n\r");
		return -1;
	}

	/* bind to specified @port */
	err = tcp_bind(pcb, IP_ADDR_ANY, port);
	if (err != ERR_OK) {
		xil_printf("Unable to bind to port %d: err = %d\n\r", port, err);
		return -2;
	}

	/* we do not need any arguments to callback functions */
	tcp_arg(pcb, NULL);

	/* listen for connections */
	pcb = tcp_listen(pcb);
	if (!pcb) {
		xil_printf("Out of memory while tcp_listen\n\r");
		return -3;
	}

	/* specify callback to use for incoming connections */
	tcp_accept(pcb, accept_callback);

	xil_printf("Capture server started @ port %d\n\r", port);

	return 0;
}
