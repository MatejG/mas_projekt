
#include <stdlib.h>
#include <malloc.h>
#include "xparameters.h"
#include "xiicps.h"
#include "xil_printf.h"
#include "xgpio.h"

#define width 640
#define height 480

unsigned char* Camera_getFrame();
XGpio cam;
#define READ_CAM XGpio_DiscreteRead(&cam,1)

#define CAMERA_isVSYNdown(a) ((a&0x0400)==0x0000)
#define CAMERA_isVSYNup(a) ((a&0x0400)==0x0400)
#define CAMERA_isHREFdown(a) ((a&0x0200)==0x0000)
#define CAMERA_isHREFup(a) ((a&0x0200)==0x0200)
#define CAMERA_isPCLKdown(a) ((a&0x0100)==0x0000)
#define CAMERA_isPCLKup(a) ((a&0x0100)==0x0100)

unsigned char* Camera_getFrame(){

	unsigned char* Y=NULL;
	int z,y,r,tt,tt1;

	//init gpio
	int status = XGpio_Initialize(&cam, XPAR_AXI_GPIO_0_DEVICE_ID);
	if (status != XST_SUCCESS) {
		print("Err: Camera gpio initalization failed\n\r");
		return NULL;
	} else {
		print("Info: Camera gpio Initialization successful\n\r");
	}

	XGpio_SetDataDirection(&cam, 1, 0xffff);

	Y = (unsigned char*)malloc(height*width*2*sizeof(unsigned char));
	if(Y==NULL){
		xil_printf("Unable to allocate memory for Y!\n\r");
		return NULL;
	}

    z=0;
	while(CAMERA_isVSYNdown(READ_CAM)){};
	while(CAMERA_isVSYNup(READ_CAM)){};

			for(y = 0; y<height; y++){ //redovi slike
				//po�etak reda slike
				do{
					tt=READ_CAM;
				}while(CAMERA_isHREFdown(tt));

				for(r = 0;r<2*width;r++){ //bajtovi slike
					do{tt=READ_CAM;}
					while(CAMERA_isPCLKdown(tt));
					do{
						tt1=READ_CAM;
					}while(CAMERA_isPCLKup(tt1)); //padaju�i brid PCLK namje�ten u COM10
					Y[z] =(u8)(tt);//valjan bajt
					z++;
				}
				while(CAMERA_isHREFup(READ_CAM));

			}
			return Y;
}
